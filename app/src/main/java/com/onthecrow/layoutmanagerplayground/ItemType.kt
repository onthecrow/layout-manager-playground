package com.onthecrow.layoutmanagerplayground

/**
 * @author ext.dvoronov
 */
enum class ItemType {
    LARGE, MEDIUM, SMALL
}
