package com.onthecrow.layoutmanagerplayground

import java.util.*

/**
 * @author ext.dvoronov
 */
object ItemsRepository {

    val items: List<DummyItem>
        get() = LinkedList<DummyItem>().apply {
            for (i in 1..1000) {
                add(DummyItem(i, ItemType.LARGE))
            }
        }
}
