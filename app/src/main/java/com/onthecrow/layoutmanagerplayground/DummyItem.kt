package com.onthecrow.layoutmanagerplayground

import java.util.*

/**
 * @author ext.dvoronov
 */
data class DummyItem(
    val index: Int,
    val itemType: ItemType,
    val title: String = "dummy item",
    val id: String = UUID.randomUUID().toString()
)
