package com.onthecrow.layoutmanagerplayground

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (gridRecycler.layoutManager as FixedGridLayoutManager).setTotalColumnCount(25)

//        (gridRecycler.layoutManager as SpannedGridLayoutManager).setSpanLookup {
//            SpannedGridLayoutManager.SpanInfo(
//                1,
//                1
//            )
//        }
        with(DummyAdapter()) {
            gridRecycler.adapter = this
            this.submitList(ItemsRepository.items)
        }
    }
}
