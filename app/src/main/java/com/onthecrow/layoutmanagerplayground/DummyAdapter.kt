package com.onthecrow.layoutmanagerplayground

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_dummy.*

/**
 * @author ext.dvoronov
 */
class DummyAdapter : ListAdapter<DummyItem, DummyAdapter.DummyViewHolder>(
    object : DiffUtil.ItemCallback<DummyItem>() {
        override fun areItemsTheSame(oldItem: DummyItem, newItem: DummyItem) =
            oldItem == newItem


        override fun areContentsTheSame(oldItem: DummyItem, newItem: DummyItem) =
            oldItem.id == newItem.id && oldItem.title == newItem.title
    }
) {

    var items: MutableList<DummyItem> = mutableListOf()
        private set

    override fun submitList(list: MutableList<DummyItem>?) {
        super.submitList(list)
        items = list ?: mutableListOf()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = DummyViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_dummy, parent, false)
    )

    override fun onBindViewHolder(holder: DummyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class DummyViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(item: DummyItem) {
            dummyIndex.text = item.index.toString()
            dummyTitle.text = item.title
            val width = if ((Math.random() * 10) >= 2) {
                containerView.context.resources.getDimensionPixelSize(R.dimen.wide_item_width)
            } else {
                containerView.context.resources.getDimensionPixelSize(R.dimen.small_item_width)
            }
            containerView.updateLayoutParams {
                this.width = width
            }
        }
    }
}
